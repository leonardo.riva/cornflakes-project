package org.cornflakes.application

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{col, lower, sha2}
import org.apache.spark.sql.types.{DoubleType, IntegerType, StringType, StructType}

object SparkJob2 {

  case class KaggleSong(kaggle_id: Int, artist_name: String, track_name: String, release_date: Int, genre: String,
                        lyrics: String, len: Int, dating: Double, violence: Double, world_life: Double,
                        night_time: Double, shake_the_audience: Double, family_gospel: Double, romantic: Double,
                        communication: Double, obscene: Double, music: Double, movement_places: Double,
                        light_visual_perceptions: Double, family_spiritual: Double, like_girls: Double, sadness: Double,
                        feelings: Double, danceability: Double, loudness: Double, acousticness: Double,
                        instrumentalness: Double, valence: Double, energy: Double, topic: String, age: Double)


  def main(args: Array[String]): Unit = {

    val spark = SparkSession
      .builder
      .appName("CornFlakesRivaLeonardo")
      .master("local[*]")
      .getOrCreate()

    // hide warnings
    spark.sparkContext.setLogLevel("ERROR")

    // read Deezer file
//    val base_dir: String = "hdfs://localhost:8020/data/"
    val base_dir: String = "./data/"

    val songs1 = spark.read.parquet(base_dir+"deezer_songs.parquet")


    val kaggleSongSchema = new StructType()
      .add("kaggle_id", IntegerType, nullable = true)
      .add("artist_name", StringType, nullable = true)
      .add("track_name", StringType, nullable = true)
      .add("release_date", IntegerType, nullable = true)
      .add("genre", StringType, nullable = true)
      .add("lyrics", StringType, nullable = true)
      .add("len", IntegerType, nullable = true)
      .add("dating", DoubleType, nullable = true)
      .add("violence", DoubleType, nullable = true)
      .add("world_life", DoubleType, nullable = true)
      .add("night_time", DoubleType, nullable = true)
      .add("shake_the_audience", DoubleType, nullable = true)
      .add("family_gospel", DoubleType, nullable = true)
      .add("romantic", DoubleType, nullable = true)
      .add("communication", DoubleType, nullable = true)
      .add("obscene", DoubleType, nullable = true)
      .add("music", DoubleType, nullable = true)
      .add("movement_places", DoubleType, nullable = true)
      .add("light_visual_perceptions", DoubleType, nullable = true)
      .add("family_spiritual", DoubleType, nullable = true)
      .add("like_girls", DoubleType, nullable = true)
      .add("sadness", DoubleType, nullable = true)
      .add("feelings", DoubleType, nullable = true)
      .add("danceability", DoubleType, nullable = true)
      .add("loudness", DoubleType, nullable = true)
      .add("acousticness", DoubleType, nullable = true)
      .add("instrumentalness", DoubleType, nullable = true)
      .add("valence", DoubleType, nullable = true)
      .add("energy", DoubleType, nullable = true)
      .add("topic", StringType, nullable = true)
      .add("age", DoubleType, nullable = true)

    // read Kaggle file
    import spark.implicits._
    val songs2 = spark.read
      .option("header", "true")
      .schema(kaggleSongSchema)
      .csv(base_dir+"/tcc_ceds_music.csv")
      .as[KaggleSong]


    val songs1formatted = songs1
      .withColumn("track_name", lower($"track_name"))
      .withColumn("artist_name", lower($"artist_name"))

    val songs2formatted = songs2
      .drop("valence")
      .withColumn("track_name", lower($"track_name"))
      .withColumn("artist_name", lower($"artist_name"))

    // aggregating the two sources
    val songs = songs1formatted.join(songs2formatted, usingColumns = Seq("track_name", "artist_name"))

    // creating hash-codes as ids for some artists
    val songsHashed = songs
      .filter(($"artist_name" === "gigi d'agostino")
        || ($"artist_name" === "bob marley")
        || ($"artist_name" === "pixies"))
      .withColumn("artist_hash", sha2($"artist_name", 256))

    // filter out artists
    val songs_filtered = songs.filter(
      ($"artist_name" =!= "katy perry")
        && ($"artist_name" =!= "taylor swift")
        && ($"artist_name" =!= "dua lipa")
    )

    // aggregating
    val songs_aggregated = songs_filtered
      .groupBy("artist_name")
      .avg("valence", "arousal", "dating", "violence", "world_life")

    // printing highest and lowest values
    for (x <- List("valence", "arousal", "dating", "violence", "world_life")) {
      println("\n"+ x)
      println("Min: " + songs_aggregated.select("artist_name", "avg("+x+")").orderBy(col("avg("+x+")")).first())
      println("Max: " + songs_aggregated.select("artist_name", "avg("+x+")").orderBy(col("avg("+x+")").desc).first())
    }

    spark.stop()

  }
}
