package org.cornflakes.application

import org.apache.spark.SparkFiles
import org.apache.spark.sql.{SaveMode, SparkSession}
import org.apache.spark.sql.types.{DoubleType, IntegerType, StringType, StructType}

object SparkJob1 {

  case class DeezerSong(deezer_song_id: Int, msd_song_id: String, msd_track_id: String, valence: Double,
                  arousal: Double, artist_name: String, track_name: String)

  def main(args: Array[String]): Unit = {

    val spark = SparkSession
      .builder
      .appName("CornFlakesRivaLeonardo")
      .master("local[*]")
      .getOrCreate()

    // hide warnings
    spark.sparkContext.setLogLevel("ERROR")

    val deezerSongSchema = new StructType()
      .add("deezer_song_id", IntegerType, nullable = true)
      .add("msd_song_id", StringType, nullable = true)
      .add("msd_track_id", StringType, nullable = true)
      .add("valence", DoubleType, nullable = true)
      .add("arousal", DoubleType, nullable = true)
      .add("artist_name", StringType, nullable = true)
      .add("track_name", StringType, nullable = true)

//        val base_dir: String = "hdfs://localhost:8020/data/"
        val base_dir: String = "./data/"

    import spark.implicits._
    val songsTrain = spark.read
      .option("header", "true")
      .schema(deezerSongSchema)
      .csv(base_dir+"train.csv")
      .as[DeezerSong]
    val songsVal = spark.read
      .option("header", "true")
      .schema(deezerSongSchema)
      .csv(base_dir+"validation.csv")
      .as[DeezerSong]
    val songsTest = spark.read
      .option("header", "true")
      .schema(deezerSongSchema)
      .csv(base_dir+"test.csv")
      .as[DeezerSong]

    // concatenate the datasets
    val songs = songsTrain.union(songsVal).union(songsTest)

    // write to file
    songs.write.mode(SaveMode.Overwrite).parquet(base_dir+"deezer_songs.parquet")

    spark.stop()
  }
}
