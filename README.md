
# CornFlakes Project
Author: Leonardo Riva


### Description
Data pipeline with Apache Spark, Docker and Hadoop, using Deezer Mood detection dataset (https://github.com/deezer/deezer_mood_detection_dataset) and Kaggle Music Dataset (https://www.kaggle.com/datasets/saurabhshahane/music-dataset-1950-to-2019). 

HADOOP NOT WORKING, USING LOCAL STORAGE INSTEAD


### Requirements
- Docker 23.0.1


### Procedure

Setup Spark code

- Assemble the JAR file with sbt: `sbt assembly`

Set up docker

- Create first image, based on bitnami/spark (copying JAR file into it): `docker build -t myspark:1.0 -f Dockerfile_spark .`

- Create second image, based on uhopper/hadoop (copying csv files into it): `docker build -t myhdfs:1.0 -f Dockerfile_hdfs .`

- Start the containers, with `docker-compose up`

Start Spark jobs

- First Spark Job: `docker exec spark_container spark-submit --class org.cornflakes.application.SparkJob1 ./target/scala-2.12/CornFlakesRivaLeonardo-assembly-0.1.0-SNAPSHOT.jar`

- Second Spark Job: `docker exec spark_container spark-submit --class org.cornflakes.application.SparkJob2 ./target/scala-2.12/CornFlakesRivaLeonardo-assembly-0.1.0-SNAPSHOT.jar`

